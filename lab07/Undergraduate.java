package lab07;

public abstract class Undergraduate extends Student{
    int credits;

    public Undergraduate(int id, String name, String major, int term, int balance, int credits){
        super(id, name, major, term, balance);
        this.credits = credits;
    }

    abstract void payTuition();

    abstract void getSummary();

}
