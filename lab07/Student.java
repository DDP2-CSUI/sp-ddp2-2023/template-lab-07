package lab07;

public abstract class Student {
    private int id;
    private String name;
    private String major;
    private int term;
    private int balance;

    public Student(int id, String name, String major, int term, int balance){
        this.id = id;
        this.name = name;
        this.major = major;
        this.term = term;
        this.balance = balance;
    }

    abstract void payTuition();

    abstract void getSummary();
}
