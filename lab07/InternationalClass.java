package lab07;

public class InternationalClass extends Undergraduate{
    
    private String countryOfChoice;
    
    public InternationalClass(int id, String name, String major, int term, int balance, int credits, String countryOfChoice){
        super(id, name, major, term, balance, credits);
        this.countryOfChoice = countryOfChoice;
    }

    public void payTuition(){

    }

    public void getSummary(){
        
    }
}
