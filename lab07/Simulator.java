package lab07;

import java.util.ArrayList;
import java.util.List;

public class Simulator {
    private List<Student> students = new ArrayList<>();
    public static void main(String[] args) {
        Simulator simulator = new Simulator();
        simulator.init();

        try {
            simulator.run();
        } catch (Exception e) {
            System.out.println(e);
        } finally {
        }
        
    }

    private void run() throws InsufficientBalanceException{
        this.nextTerm();
        this.nextTerm();

        System.out.println("GRADUATION - 1");
        for (Student student : students) {
            student.graduate();
        }
        System.out.println();

        this.nextTerm();
        this.nextTerm();
        this.nextTerm();

        Thesis sabie = (Thesis) students.get(2);
        Thesis bimbek = (Thesis) students.get(3);
        Thesis indri = (Thesis) students.get(5);
        sabie.writeThesis("Analisis Pengaruh Tiktok terhadap Motivasi Belajar");
        bimbek.writeThesis("Analisis Pengaruh Penggunaan ChatGPT terhadap Kemampuan Pemrograman");
        indri.writeThesis("Pengembangan Website Ujian di Fuzzylkom");

        System.out.println();

        this.nextTerm();

        System.out.println("GRADUATION - 2");
        for (Student student : students) {
            student.graduate();
        }
        System.out.println();


        this.nextTerm();
        System.out.println("GRADUATION - 3");
        for (Student student : students) {
            student.graduate();
        }
        System.out.println();
    }


    private void nextTerm() throws InsufficientBalanceException {
        for (Student student : students) {
            student.setActive(false);
            student.payTuition();
            student.addTerm();
        }
    }

    private void init() {
        Student iky = new InternationalClass(12345, "Rizky Pratama", "Information System", 2, Integer.MAX_VALUE, 20, "Australia");
        Student kika = new Regular(145, "Erika Humairah", "Computer Science", 0, Integer.MAX_VALUE, 20);
        Student sabie = new Postgraduate(190, "Shabrina Nataprawira", "Information Technology", 0, Integer.MAX_VALUE, "Cybersecurity");
        Student bimbek = new Postgraduate(188, "Bima Baginda", "Information System", 2, Integer.MAX_VALUE, "eCommerce");
        Student bitty = new InternationalClass(1818, "Basyira Shabiqa", "Computer Science", 0, Integer.MAX_VALUE, 22, "UK");
        Student indri = new Regular(1001, "Indri Klarinet", "Computer Science", 2, Integer.MAX_VALUE, 22);

        students.add(iky);
        students.add(kika);
        students.add(sabie);
        students.add(bimbek);
        students.add(bitty);
        students.add(indri);
    }
    
}
